![image](https://user-images.githubusercontent.com/78088608/142757436-9bdc54cc-fa35-405f-98eb-5be4e8f7153c.png)

- 👋 Hi, I’m Jakub Kolář aka @Jakewh. Made in Czech republic in 1987.
- 👀 I’m interested in Python. I am also interested in vineyards and wine production, modern technology, Linux and open source.
- 🌱 I am currently learning to better understand Python. In the future, I plan to deal with SQL and Qt.
- 💞️ I’m looking to collaborate on ...?
- 📫 You can contact me at kolarkuba@gmail.com
<HR>
<blockquote>
      <p>It's easier to believe in something than to understand something.</p>
    </blockquote>
<HR>
